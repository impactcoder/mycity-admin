	function acknowledgeComplaint(complaintId){
		console.log("ComplaintId :"+complaintId);
		$.post("http://3dbb46ca.ngrok.com/complaint/ack",{complaintId: complaintId},function(data,status){
			console.log("Data :"+data+" Status :"+status);
		});
	}

	function rejectComplaint(complaintId){
		$.post("http://3dbb46ca.ngrok.com/complaint/rej",{complaintId: complaintId},function(data,status){
			console.log("Data :"+data+" Status :"+status);
		});	
	}

	function loadCustomReports(){
		$('.sub-header').hide();
		$('.table-responsive').hide();

		$.get("http://3dbb46ca.ngrok.com/complaint/typeGrouping",function(data,status){
			typeData(data);
		});
		
		$.get("http://3dbb46ca.ngrok.com/complaint/statusGrouping",function(data,status){
			statusData(data);
		});
	}

	function typeData (chartData) {
 	  $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Defect Type Split Up'
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Type share',
            data: chartData
        }]
  	  });
	}

	function statusData (chartData) {
 	  $('#statusContainer').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Status Type Split Up'
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Status share',
            data: chartData
        }]
  	  });
	}

	function loadSubmittedComplaints(){
			$("#container").hide();
			$("#submittedComplaints").hide();
			$("#allComplaints").hide();
		 	$.get("http://3dbb46ca.ngrok.com/complaint/submitted",function(data,status){
		 		$("#submittedComplaints").find("tr:gt(0)").remove();
				$("#submittedComplaints").append("<tbody>");
				var count = 1;
				$.each(data,function(){
					var image = "<image src='https://s3-ap-southeast-1.amazonaws.com/mycitycomplaintimages/"+this.image+"' style='width:100px;height:100px'/>";
					var rowData = "<td>"+(count++)+"</td>"+"<td>"+image+"</td>"+"<td>"+this.title+"</td>"+"<td>"+this.desc+"</td>"+"<td>"+this.complaintType+"</td>"+"<td>"+this.status+"</td>";
					rowData += "<td><input type=\"button\" value=\"ACK\" id=\""+this._id+"\"></input>";
					rowData += "<td><input type=\"button\" value=\"REJ\" id=\""+this._id+"\"></input>";
					$("#submittedComplaints").append("<tr>"+rowData+"</tr>");				
				});
				$("#submittedComplaints").append("</tbody>");
				$("input[value='ACK']").click(function(){
	    			var complaintId = $(this).attr('id');	   
 					var parent = $(this).parent();
 					parent.prev('td').html('ACK');
 					var tickImage = "<img src='http://www.polyvore.com/cgi/img-thing?.out=jpg&size=l&tid=17926277' width='25' height='25'/>"
					parent.html(tickImage);
 					$.post("http://3dbb46ca.ngrok.com/complaint/ack",{complaintId: complaintId},function(data,status){
					});
				});
				$("#submittedComplaints").show();
  			});
	}

	function loadAcknowledgedComplaints(){
			$("#container").hide();
			$("#submittedComplaints").hide();
			$("#allComplaints").hide();
		 	$.get("http://3dbb46ca.ngrok.com/complaint/acknowledged",function(data,status){
				$("#acknowledgedComplaints").find("tr:gt(0)").remove();
				$("#acknowledgedComplaints").append("<tbody>");
				var count = 1;
				$.each(data,function(){
					var image = "<image src='https://s3-ap-southeast-1.amazonaws.com/mycitycomplaintimages/"+this.image+"' style='width:100px;height:100px'/>";
					var rowData = "<td>"+(count++)+"</td>"+"<td>"+image+"</td>"+"<td>"+this.title+"</td>"+"<td>"+this.desc+"</td>"+"<td>"+this.complaintType+"</td>"+"<td>"+this.status+"</td>";
					$("#acknowledgedComplaints").append("<tr>"+rowData+"</tr>");				
				});
				$("#acknowledgedComplaints").append("</tbody>");
				$("#acknowledgedComplaints").show();
  			});
	}
	
	$(document).ready(function(){
			$("#submittedComplaints").hide();
			$("#acknowledgedComplaints").hide();
			$.get("http://3dbb46ca.ngrok.com/complaints",function(data,status){
				$("#allComplaints").append("<tbody>");
				var count = 1;
				$.each(data,function(){
					var image = "<image src='https://s3-ap-southeast-1.amazonaws.com/mycitycomplaintimages/"+this.image+"' style='width:100px;height:100px'/>";
					var rowData = "<td>"+(count++)+"</td>"+"<td>"+image+"</td>"+"<td>"+this.title+"</td>"+"<td>"+this.desc+"</td>"+"<td>"+this.complaintType+"</td>"+"<td>"+this.status+"</td>";
					$("#allComplaints").append("<tr>"+rowData+"</tr>");				
				});
				$("#allComplaints").append("</tbody>");
  			});


			$("input[value='REJ']").on('click',function(){
	    		var complaintId = $(this).attr('id');	   
 				var parent = $(this).parent();
 				$.post("http://3dbb46ca.ngrok.com/complaint/rej",{complaintId: complaintId},function(data,status){
					parent.prev('td').html('REJ');
 					var failImage = "<img src='http://www.iconsdb.com/icons/preview/black/x-mark-m.png' width='25' height='25'/>"
					parent.html(failImage);
				});
		});
	});